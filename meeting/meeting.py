from __future__ import unicode_literals
from frappe import _

def get_data():
    return [
      {
        "label":_("meeting"),
        "icon": "octicon octicon-briefcase",
        "items": [
            {
              "type": "doctype",
              "name": "meeting",
              "label": _("meeting"),
              "description": _("meeting which members issue and return."),
            }
	}
       
  ]
