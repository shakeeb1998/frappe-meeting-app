from __future__ import unicode_literals
from frappe import _

def get_data():
    return [
      {
        "label":_("Meeting"),
        "icon": "octicon octicon-briefcase",
        "items": [
            {
              "type": "doctype",
              "name": "Meeting",
              "label": _("meeting"),
              "description": _("meeting which members issue and return."),
            },
            {
              "type": "doctype",
              "name": "Task List",
              "label": _("Task List"),
              "description": _("meeting2 which members issue and return."),
            },
            {
              "type": "doctype",
              "name": "Task",
              "label": _("Task"),
              "description": _("meetingasd which members issue and return."),
            }
       ]
}
  ]
